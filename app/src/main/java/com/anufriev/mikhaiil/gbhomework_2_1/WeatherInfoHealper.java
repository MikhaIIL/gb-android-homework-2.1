package com.anufriev.mikhaiil.gbhomework_2_1;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class WeatherInfoHealper {
    private static final String WEATHER_URL_PATTERN = "https://api.openweathermap.org/data/2.5/weather?q=%s&units=metric&appid=%s";
    private static final String weatherAPIKey = "1f87966576e55cdcec27ea445037daa8";
    private static final String RESPONSE_STATUS = "cod";
    private static final int RESPOSE_OK = 200;

    public static JSONObject getWeatherInfoByCity(String cityName) {
        try {
            URL fullURL = new URL(String.format(WEATHER_URL_PATTERN, cityName, weatherAPIKey));
            HttpURLConnection connection = (HttpURLConnection) fullURL.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            Scanner scanner = new Scanner(reader);
            StringBuilder weatherRawData = new StringBuilder();
            while (scanner.hasNextLine()) {
                weatherRawData.append(scanner.nextLine()).append("\n");
            }
            scanner.close();
            reader.close();

            JSONObject responseWeatherObj = new JSONObject(weatherRawData.toString());

            if (responseWeatherObj.getInt(RESPONSE_STATUS) == RESPOSE_OK) {
                return responseWeatherObj;
            } else return null;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
