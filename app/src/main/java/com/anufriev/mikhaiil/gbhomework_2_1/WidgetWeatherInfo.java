package com.anufriev.mikhaiil.gbhomework_2_1;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.widget.RemoteViews;

import com.google.gson.Gson;

import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

public class WidgetWeatherInfo extends AppWidgetProvider {
    private SharedPreferences pref;
    private String cityIdent;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        super.onUpdate(context, appWidgetManager, appWidgetIds);
        for (int i : appWidgetIds) {
            updateWidget(context, appWidgetManager, i);
        }
    }

    public void updateWidget(final Context context, final AppWidgetManager appWidgetManager, final int widgetId) {

        new Thread(new Runnable() {
            @Override
            public void run() {
                RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.layout_widget);

                String sharedPrefName = context.getResources().getString(R.string.shared_pref_name);
                pref = context.getSharedPreferences(sharedPrefName, MODE_PRIVATE);
                cityIdent = context.getResources().getString(R.string.preference_key_city);
                String lastSavedCity = Scrambler.getScrambler().getWord(pref.getString(cityIdent, "defVal"), false);

                final JSONObject jsonWeatherObj = WeatherInfoHealper.getWeatherInfoByCity(lastSavedCity);
                if (jsonWeatherObj != null) {
                    WeatherInfo weatherInfo = new Gson().fromJson(jsonWeatherObj.toString(), WeatherInfo.class);
                    remoteViews.setImageViewUri(R.id.widget_image, Uri.parse(weatherInfo.getWeather()[0].getWeatherIconURI()));
                    remoteViews.setTextViewText(R.id.widget_textview, lastSavedCity + ", " + "\n" + weatherInfo.getWeather()[0].getWeatherDescription() + ", " +
                            String.valueOf(weatherInfo.getMainInfo().getMainTemperature()) + context.getResources().getString(R.string.celsius_degree));
                }
                appWidgetManager.updateAppWidget(widgetId, remoteViews);
            }
        }).start();
    }
}
