package com.anufriev.mikhaiil.gbhomework_2_1;

public class Scrambler {

    private static final String[][] box_1 = {{"Q", "W", "E", "R", "T", "1"}, {"Y", "U", "I", "O", "P", "2"}
            , {"A", "S", "D", "F", "G", "3"}, {"H", "J", "K", "L", "Z", "4"}
            , {"N", "X", "C", "V", "B", "5"}, {"6", "7", "8", "M", "9", "0"}};
    private static final String[][] box_2 = {{"Q", "W", "E", "R", "T", "2"}, {"N", "X", "C", "V", "B", "4"}
            , {"A", "S", "D", "F", "G", "1"}, {"H", "J", "K", "L", "Z", "3"}
            , {"Y", "U", "I", "O", "P", "6"}, {"5", "7", "8", "0", "9", "M"}};
    private static final String[][] box_3 = {{"Q", "W", "E", "R", "T", "3"}, {"Y", "U", "I", "O", "P", "1"}
            , {"N", "X", "C", "V", "B", "7"}, {"H", "J", "K", "L", "Z", "M"}
            , {"A", "S", "D", "F", "G", "8"}, {"5", "6", "4", "0", "9", "2"}};
    private static final String[][] box_4 = {{"A", "S", "D", "F", "G", "9"}, {"H", "J", "K", "L", "Z", "M"}
            , {"Q", "W", "E", "R", "T", "0"}, {"Y", "U", "I", "O", "P", "1"}
            , {"N", "X", "C", "V", "B", "7"}, {"5", "6", "4", "8", "3", "2"}};
    private static int arrSize = 6;
    private static Scrambler scrambler;

    public static Scrambler getScrambler() {
        if (scrambler == null) {
            scrambler = new Scrambler();
        }
        return scrambler;
    }

    private static int[] findCoordInArr(String[][] arr, String strToFind) {
        int[] res = new int[2];
        for (int i = 0; i < arrSize; i++) {
            for (int j = 0; j < arrSize; j++) {
                if (arr[i][j].equals(strToFind)) {
                    res[0] = i;
                    res[1] = j;
                    return res;
                }
            }
        }
        return null;
    }

    private static String encodeSymbols(int[] firstSymb, int[] secondSymb) {
        return box_2[secondSymb[0]][firstSymb[1]] + box_3[firstSymb[0]][secondSymb[1]];
    }

    private static String decodeSymbols(int[] firstSymb, int[] secondSymb) {
        return box_1[secondSymb[0]][firstSymb[1]] + box_4[firstSymb[0]][secondSymb[1]];
    }

    public String getWord(String str, boolean isEncoding) {
        String[] inputStr = str.toUpperCase().split("");
        String result = "";
        int counter = 1;/*т.к. 0 элемент пустой*/

        while (inputStr.length - counter > 1) {
            if (isEncoding) {
                result += encodeSymbols(findCoordInArr(box_1, inputStr[counter]),
                        findCoordInArr(box_4, inputStr[counter + 1]));
            } else {
                result += decodeSymbols(findCoordInArr(box_2, inputStr[counter]),
                        findCoordInArr(box_3, inputStr[counter + 1]));
            }

            counter += 2;
        }
        if (inputStr.length - counter == 1) {
            result += inputStr[counter];
        }

        return result.toLowerCase();
    }

}
