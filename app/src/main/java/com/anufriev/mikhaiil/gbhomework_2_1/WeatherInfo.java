package com.anufriev.mikhaiil.gbhomework_2_1;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WeatherInfo {

    private static String DEFAULT_ICON_PATH = "http://openweathermap.org/img/w/";
    private static final String DRAWABLE_PATH = "android.resource://com.anufriev.mikhaiil.gbhomework_2_1/drawable/";

    @SerializedName("name")
    private String cityName;
    @SerializedName("cod")
    private int responseCode;
    @SerializedName("base")
    private String baseParams;
    @SerializedName("dt")
    private long dateCalc;
    @SerializedName("id")
    private int cityID;

    @SerializedName("sys")
    private SysData sysData;
    @SerializedName("weather")
    private Weather[] weather;
    @SerializedName("main")
    private MainInfo mainInfo;
    @SerializedName("wind")
    private Wind wind;
    @SerializedName("clouds")
    private Clouds clouds;
    @SerializedName("coord")
    private CoordData coordData;
    @SerializedName("rain")
    private Rain rain;
    @SerializedName("snow")
    private Snow snow;

    public WeatherInfo() {
        weather = new Weather[1];
        weather[0] = new Weather();
        mainInfo = new MainInfo();
        wind = new Wind();
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getCityID() {
        return cityID;
    }

    public void setCityID(int cityID) {
        this.cityID = cityID;
    }

    public String getCityName() {
        return cityName;
    }

    public String getDateCalc() {
        /*приходит в секундах, переводим в миллисекунды*/
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(dateCalc * 1000));
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getBaseParams() {
        return baseParams;
    }

    public void setDateCalc(long dateCalc) {
        this.dateCalc = dateCalc;
    }

    public SysData getSysData() {
        return sysData;
    }

    public MainInfo getMainInfo() {
        return mainInfo;
    }

    public Wind getWind() {
        return wind;
    }

    public Clouds getClouds() {
        return clouds;
    }

    public Weather[] getWeather() {
        return weather;
    }

    /*coord*/
    class CoordData {
        @SerializedName("lon")
        private double longitude;
        @SerializedName("lat")
        private double latitude;
    }

    class Weather {
        @SerializedName("id")
        private int weatherConditionID;
        @SerializedName("main")
        private String weatherMainParams;
        @SerializedName("description")
        private String weatherDescription;
        @SerializedName("icon")
        private String weatherIconID;

        public String getWeatherIconSRC() {
            return DEFAULT_ICON_PATH + weatherIconID + ".png";
        }

        public String getWeatherDescription() {
            return weatherDescription;
        }

        public String getWeatherIconID() {
            return weatherIconID;
        }


        public String getWeatherIconURI() {
            String result;
            switch (weatherIconID) {
                case "01d":
                    result = DRAWABLE_PATH + "sunny";
                    break;
                case "02d":
                    result = DRAWABLE_PATH + "cloudy1";
                    break;
                case "03d":
                    result = DRAWABLE_PATH + "cloudy5";
                    break;
                case "04d":
                    result = DRAWABLE_PATH + "overcast";
                    break;
                case "10d":
                    result = DRAWABLE_PATH + "tstorm1";
                    break;
                case "11d":
                    result = DRAWABLE_PATH + "tstorm2";
                    break;
                case "09d":
                    result = DRAWABLE_PATH + "tstorm3";
                    break;
                case "13d":
                    result = DRAWABLE_PATH + "snow4";
                    break;
                case "50d":
                    result = DRAWABLE_PATH + "mist";
                    break;

                default:
                    result = DRAWABLE_PATH + "dunno";
                    break;
            }
            return result;
        }

        public void setWeatherDescription(String weatherDescription) {
            this.weatherDescription = weatherDescription;
        }

        public void setWeatherIconID(String weatherIconID) {
            this.weatherIconID = weatherIconID;
        }
    }

    class MainInfo {
        @SerializedName("temp")
        private double mainTemperature;
        @SerializedName("pressure")
        private double mainPressure;
        @SerializedName("humidity")
        private double mainHumidity;
        @SerializedName("temp_min")
        private double mainTemperatureMin;
        @SerializedName("temp_max")
        private double mainTemperatureMax;

        public void setMainTemperature(double mainTemperature) {
            this.mainTemperature = mainTemperature;
        }

        public void setMainPressure(double mainPressure) {
            this.mainPressure = mainPressure;
        }

        public double getMainTemperature() {
            return mainTemperature;
        }

        public double getMainPressure() {
            return mainPressure;
        }
    }

    class Wind {
        @SerializedName("speed")
        private double windSpeed;
        @SerializedName("deg")
        private double windDegrees;

        public void setWindSpeed(double windSpeed) {
            this.windSpeed = windSpeed;
        }

        public double getWindSpeed() {
            return windSpeed;
        }
    }

    class Clouds {
        @SerializedName("all")
        private double cloudAll;
    }

    class Rain {
        @SerializedName("3h")
        private double rain3h;
    }

    class Snow {
        @SerializedName("3h")
        private double snow3h;
    }

    class SysData {
        @SerializedName("type")
        private int sysType;
        @SerializedName("id")
        private Integer sysID;
        @SerializedName("message")
        private String sysMessage;
        @SerializedName("country")
        private String sysCountry;
        @SerializedName("sunrise")
        private long sysDateSunrise;
        @SerializedName("sunset")
        private long sysDateSunset;
    }
}
