package com.anufriev.mikhaiil.gbhomework_2_1;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

public class ExtendedBroadcastReceiver extends BroadcastReceiver {
    private static final int PENDINGINTENT_REQUEST_CODE = 29075;
    private static ArrayList<String> cityArr;

    public ExtendedBroadcastReceiver() {
    }

    public ExtendedBroadcastReceiver(Context context, int timeoutInSeconds) {
        Intent intent = new Intent(context, ExtendedBroadcastReceiver.class);

        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(System.currentTimeMillis());
        time.add(Calendar.SECOND, timeoutInSeconds);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, PENDINGINTENT_REQUEST_CODE, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.setRepeating(AlarmManager.RTC, time.getTimeInMillis(), AlarmManager.INTERVAL_HOUR * 3, pendingIntent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final WeatherDataSource weatherDS = new WeatherDataSource(context);
        new Thread() {
            public void run() {
                WeatherInfo weatherInfo;

                try {
                    weatherDS.open();
                    cityArr = weatherDS.getCityList();
                    for (String cityName : cityArr) {
                        final JSONObject jsonWeatherObj = WeatherInfoHealper.getWeatherInfoByCity(cityName);
                        if (jsonWeatherObj != null) {
                            weatherInfo = new Gson().fromJson(jsonWeatherObj.toString(), WeatherInfo.class);
                            if (!weatherDS.isCityDataActual(cityName)) {
                                weatherDS.updateWeatherInfo(weatherInfo);
                            }
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                } finally {
                    weatherDS.close();
                }
            }
        }.start();

        Toast.makeText(context, "Updating weather from server...", Toast.LENGTH_SHORT).show();
    }

    public void disableReceiver(Context context) {
        Intent intent = new Intent(context, ExtendedBroadcastReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, PENDINGINTENT_REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.cancel(pendingIntent);
    }


}
