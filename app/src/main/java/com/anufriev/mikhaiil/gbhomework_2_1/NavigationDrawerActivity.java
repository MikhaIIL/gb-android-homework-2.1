package com.anufriev.mikhaiil.gbhomework_2_1;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class NavigationDrawerActivity extends AppCompatActivity {

    private static final String DRAWABLE_PATH = "android.resource://com.anufriev.mikhaiil.gbhomework_2_1/drawable/";
    @BindView(R.id.city_name_textview)
    TextView cityNameTextView;
    @BindView(R.id.temperature_textview)
    TextView tempTextView;
    @BindView(R.id.interesting_image_view)
    ImageView imageView;
    @BindView(R.id.speed_of_wind_textview)
    TextView windTextView;
    @BindView(R.id.pressure_textview)
    TextView pressureTextView;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private NavigationView.OnNavigationItemSelectedListener listener;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    private static final int REQUEST_CODE = 1;
    private static final String FILE_NAME = "user_avatar.png";
    //private SharedPreferences pref;
    private SharedPreferences sharedPrefs;
    private String cityIdent;
    private String storageIdent;
    private String serviceStateIdent;
    private boolean isInternal;
    private boolean isServiceEnabled;
    private CircleImageView avatarImage;
    private WeatherDataSource weatherDS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weatherDS = new WeatherDataSource(this);
        try {
            weatherDS.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        initUI();
        initNavigationView();
        initAvatarImageView();
        startService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        weatherDS.close();
    }

    public void initUI() {
        setContentView(R.layout.activity_navigation_drawer);
        initPreferences();
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        String lastSavedCity = Scrambler.getScrambler().getWord(sharedPrefs.getString(cityIdent, "defVal"), false);
        if (!lastSavedCity.isEmpty()) {
            findCityInfoByName(lastSavedCity);
        }
    }

    public void initPreferences() {
        //pref = getPreferences(MODE_PRIVATE);
        String sharedPrefName = getResources().getString(R.string.shared_pref_name);
        sharedPrefs = getSharedPreferences(sharedPrefName, MODE_PRIVATE);
        cityIdent = getResources().getString(R.string.preference_key_city);
        storageIdent = getResources().getString(R.string.shared_pref_key_storage);
        serviceStateIdent = getResources().getString(R.string.shared_pref_key_service_state);

        updateSharedPrefs();
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putBoolean(storageIdent, isInternal);/*Если значения нет - заполняем по умолчанию*/
        editor.putBoolean(serviceStateIdent, isServiceEnabled);
        editor.commit();
    }

    private void updateSharedPrefs() {
        isInternal = sharedPrefs.getBoolean(storageIdent, true);
        isServiceEnabled = sharedPrefs.getBoolean(serviceStateIdent, true);
    }

    private void loadStoredAvatar(String path) {
        File file = new File(path, FILE_NAME);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        try {
            Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file));
            avatarImage.setImageBitmap(bitmap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void initAvatarImageView() {
        View headerView = navigationView.getHeaderView(0);
        avatarImage = headerView.findViewById(R.id.avatar_image_view);/*Bind не работает к сожалению*/
        if (isInternal) {
            loadStoredAvatar(getApplicationContext().getFilesDir().getPath());
        } else {
            if (isExternalStorageReadable()) {
                loadStoredAvatar(getApplicationContext().getExternalFilesDir(null).getPath());
            }
        }

        avatarImage.setClickable(true);
        avatarImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateSharedPrefs();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            Uri resultImage = data.getData();
            try {
                Bitmap image = MediaStore.Images.Media.getBitmap(getContentResolver(), resultImage);
                avatarImage.setImageBitmap(image);
                if (isInternal) {
                    File appDir = getApplicationContext().getFilesDir();
                    File internalDirectory;
                    if (appDir != null) {
                        internalDirectory = new File(appDir.getPath());
                        saveUserAvatar(internalDirectory, image);
                    }
                } else {
                    if (isExternalStorageWritable()) {
                        File externalDirectory = new File(getApplicationContext().getExternalFilesDir(null).getPath());
                        saveUserAvatar(externalDirectory, image);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void saveUserAvatar(File appDirectory, Bitmap image) {
        boolean isFileExcists = appDirectory.exists() || appDirectory.mkdirs();
        if (isFileExcists) {
            File avatarFile = new File(appDirectory, FILE_NAME);
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(avatarFile);
                image.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void initNavigationView() {
        setNavigationViewListener(new NavigationView.OnNavigationItemSelectedListener() {
            @SuppressWarnings("StatementWithEmptyBody")
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();

                if (id == R.id.nav_camera) {

                } else if (id == R.id.nav_gallery) {

                } else if (id == R.id.nav_slideshow) {

                } else if (id == R.id.nav_manage) {
                    Intent settingsActivity = new Intent(NavigationDrawerActivity.this, SettingsActivity.class);
                    NavigationDrawerActivity.this.startActivity(settingsActivity);
                } else if (id == R.id.nav_share) {

                } else if (id == R.id.nav_send) {

                }
                drawer.closeDrawer(GravityCompat.START);
                return true;
            }
        });
        navigationView.setNavigationItemSelectedListener(getListener());
    }

    public void setNavigationViewListener(@Nullable NavigationView.OnNavigationItemSelectedListener listener) {
        this.listener = listener;
    }

    public NavigationView.OnNavigationItemSelectedListener getListener() {
        return listener;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_find_by_city) {
            showInputCityDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showInputCityDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(R.string.enter_city_name);
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        dialogBuilder.setView(input);
        dialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                findCityInfoByName(input.getText().toString());
            }
        });
        dialogBuilder.show();
    }

    public void findCityInfoByName(String cityName) {
        loadWeatherInfo(cityName);
    }

    private void loadWeatherInfo(final String cityName) {
        new Thread() {
            public void run() {
                WeatherInfo weatherInfo;

                if (weatherDS.isCityLoaded(cityName) && weatherDS.isCityDataActual(cityName)) {
                    weatherInfo = weatherDS.loadWeatherInfo(cityName);
                    setViewResources(weatherInfo);
                } else {

                    final JSONObject jsonWeatherObj = WeatherInfoHealper.getWeatherInfoByCity(cityName);
                    if (jsonWeatherObj != null) {
                        weatherInfo = new Gson().fromJson(jsonWeatherObj.toString(), WeatherInfo.class);
                        if (weatherDS.isCityLoaded(cityName) && !weatherDS.isCityDataActual(cityName)) {
                            weatherDS.updateWeatherInfo(weatherInfo);
                        } else {
                            weatherDS.insertWeatherInfo(weatherInfo);
                        }
                        setViewResources(weatherInfo);
                    } else {
                        showMessage();
                    }
                }
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(cityIdent, Scrambler.getScrambler().getWord(cityName, true));
                editor.commit();

            }
        }.start();
    }

    private void setViewResources(WeatherInfo weatherInfo) {
        setText(cityNameTextView, weatherInfo.getCityName());
        setText(tempTextView, weatherInfo.getWeather()[0].getWeatherDescription() + ", " +
                String.valueOf(weatherInfo.getMainInfo().getMainTemperature()) + getResources().getString(R.string.celsius_degree));
        setText(windTextView, String.valueOf(getResources().getString(R.string.wind_word)) +
                weatherInfo.getWind().getWindSpeed() +
                getResources().getString(R.string.speed_of_wind_metric));
        setText(pressureTextView, String.valueOf(getResources().getString(R.string.pressure_word)) +
                String.valueOf(weatherInfo.getMainInfo().getMainPressure()) +
                getResources().getString(R.string.pressure_metric));
        setImageRes(imageView, weatherInfo.getWeather()[0].getWeatherIconID());
    }

    private void setText(final TextView text, final String value) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text.setText(value);
            }
        });
    }

    private void setImageRes(final ImageView imageView, final String imageFileName) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (imageFileName) {
                    case "01d":
                        imageView.setImageURI(Uri.parse(DRAWABLE_PATH + "sunny"));
                        break;
                    case "02d":
                        imageView.setImageURI(Uri.parse(DRAWABLE_PATH + "cloudy1"));
                        break;
                    case "03d":
                        imageView.setImageURI(Uri.parse(DRAWABLE_PATH + "cloudy5"));
                        break;
                    case "04d":
                        imageView.setImageURI(Uri.parse(DRAWABLE_PATH + "overcast"));
                        break;
                    case "10d":
                        imageView.setImageURI(Uri.parse(DRAWABLE_PATH + "tstorm1"));
                        break;
                    case "11d":
                        imageView.setImageURI(Uri.parse(DRAWABLE_PATH + "tstorm2"));
                        break;
                    case "09d":
                        imageView.setImageURI(Uri.parse(DRAWABLE_PATH + "tstorm3"));
                        break;
                    case "13d":
                        imageView.setImageURI(Uri.parse(DRAWABLE_PATH + "snow4"));
                        break;
                    case "50d":
                        imageView.setImageURI(Uri.parse(DRAWABLE_PATH + "mist"));
                        break;

                    default:
                        imageView.setImageURI(Uri.parse(DRAWABLE_PATH + "dunno"));
                        break;
                }
            }
        });
    }

    private void showMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "City is't found!", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public void startService() {
        if (isServiceEnabled) {
            ExtendedBroadcastReceiver alarm = new ExtendedBroadcastReceiver(this, 10);
        }

    }

}