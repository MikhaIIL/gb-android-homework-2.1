package com.anufriev.mikhaiil.gbhomework_2_1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class WeatherDataSource {
    private static final int HOUR_IN_MILLS = 3600000;
    private DataBaseHelper dbHelper;
    private SQLiteDatabase db;

    public WeatherDataSource(Context context) {
        dbHelper = new DataBaseHelper(context);
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void insertWeatherInfo(WeatherInfo infoRow) {
        ContentValues values = new ContentValues();
        values.put(DataBaseHelper.COLUMN_CITY_NAME, infoRow.getCityName());
        values.put(DataBaseHelper.COLUMN_CITY_CODE, infoRow.getCityID());
        values.put(DataBaseHelper.COLUMN_ICON_ID, infoRow.getWeather()[0].getWeatherIconID());
        values.put(DataBaseHelper.COLUMN_DESCRIPTION, infoRow.getWeather()[0].getWeatherDescription());
        values.put(DataBaseHelper.COLUMN_TEMP, infoRow.getMainInfo().getMainTemperature());
        values.put(DataBaseHelper.COLUMN_PRESS, infoRow.getMainInfo().getMainPressure());
        values.put(DataBaseHelper.COLUMN_WIND_SPEED, infoRow.getWind().getWindSpeed());
        values.put(DataBaseHelper.COLUMN_CALC_DATE, infoRow.getDateCalc());
        db.insert(DataBaseHelper.WEATHER_INFO_TABLE, null, values);
    }

    public void updateWeatherInfo(WeatherInfo infoRow) {
        ContentValues values = new ContentValues();
        values.put(DataBaseHelper.COLUMN_CITY_NAME, infoRow.getCityName());
        values.put(DataBaseHelper.COLUMN_ICON_ID, infoRow.getWeather()[0].getWeatherIconID());
        values.put(DataBaseHelper.COLUMN_DESCRIPTION, infoRow.getWeather()[0].getWeatherDescription());
        values.put(DataBaseHelper.COLUMN_TEMP, infoRow.getMainInfo().getMainTemperature());
        values.put(DataBaseHelper.COLUMN_PRESS, infoRow.getMainInfo().getMainPressure());
        values.put(DataBaseHelper.COLUMN_WIND_SPEED, infoRow.getWind().getWindSpeed());
        values.put(DataBaseHelper.COLUMN_CALC_DATE, infoRow.getDateCalc());

        db.update(DataBaseHelper.WEATHER_INFO_TABLE, values
                , DataBaseHelper.COLUMN_CITY_CODE + " = " +
                        infoRow.getCityID(), null);
    }

    public synchronized boolean isCityLoaded(String cityName) {
        Cursor cursor = db.rawQuery("SELECT count(1) FROM "
                + DataBaseHelper.WEATHER_INFO_TABLE + " WHERE lower("
                + DataBaseHelper.COLUMN_CITY_NAME + ") = '"
                + cityName.toLowerCase() + "'", null);
        cursor.moveToFirst();
        int result = cursor.getInt(0);
        cursor.close();
        return result != 0;
    }

    public ArrayList<String> getCityList() {
        ArrayList<String> arr = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT " + DataBaseHelper.COLUMN_CITY_NAME
                + " FROM " + DataBaseHelper.WEATHER_INFO_TABLE, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            arr.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return arr;
    }

    public synchronized boolean isCityDataActual(String cityName) {
        Cursor cursor = db.rawQuery("SELECT count(1) FROM "
                + DataBaseHelper.WEATHER_INFO_TABLE + " WHERE lower("
                + DataBaseHelper.COLUMN_CITY_NAME + ") = '"
                + cityName.toLowerCase() + "' AND "
                + DataBaseHelper.COLUMN_CALC_DATE + " < datetime('"
                + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(System.currentTimeMillis() - HOUR_IN_MILLS)) + "')", null);
        cursor.moveToFirst();
        int result = cursor.getInt(0);
        cursor.close();
        return result == 0;
    }

    public synchronized WeatherInfo loadWeatherInfo(String cityName) {
        WeatherInfo weatherInfo = new WeatherInfo();
        Cursor cursor = db.rawQuery("SELECT "
                + DataBaseHelper.COLUMN_CITY_NAME + ", "
                + DataBaseHelper.COLUMN_CITY_CODE + ", "
                + DataBaseHelper.COLUMN_ICON_ID + ", "
                + DataBaseHelper.COLUMN_DESCRIPTION + ", "
                + DataBaseHelper.COLUMN_TEMP + ", "
                + DataBaseHelper.COLUMN_PRESS + ", "
                + DataBaseHelper.COLUMN_WIND_SPEED + ", "
                + DataBaseHelper.COLUMN_CALC_DATE
                + " FROM " + DataBaseHelper.WEATHER_INFO_TABLE
                + " WHERE lower(" + DataBaseHelper.COLUMN_CITY_NAME
                + ") = '" + cityName.toLowerCase() + "'", null);
        cursor.moveToFirst();
        weatherInfo.setCityName(cursor.getString(0));
        weatherInfo.setCityID(cursor.getInt(1));
        weatherInfo.getWeather()[0].setWeatherIconID(cursor.getString(2));
        weatherInfo.getWeather()[0].setWeatherDescription(cursor.getString(3));
        weatherInfo.getMainInfo().setMainTemperature(cursor.getDouble(4));
        weatherInfo.getMainInfo().setMainPressure(cursor.getDouble(5));
        weatherInfo.getWind().setWindSpeed(cursor.getDouble(6));
        cursor.close();
        return weatherInfo;
    }
}
