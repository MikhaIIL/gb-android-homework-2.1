package com.anufriev.mikhaiil.gbhomework_2_1;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;

public class SettingsActivity extends AppCompatActivity {


    @BindView(R.id.rb_group_storage)
    RadioGroup storageGroup;
    @BindView(R.id.rb_group_service)
    RadioGroup serviceGroup;

    private SharedPreferences sharedPrefs;
    private String storageIdent;
    private String serviceStateIdent;
    private boolean isInternal;
    private boolean isServiceEnabled;
    private Bundle innerBundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        innerBundle = savedInstanceState;
        initUI();
        initPreferences();
        initRadioButtons();

    }

    public void initUI() {

        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void initPreferences() {
        String sharedPrefName = getResources().getString(R.string.shared_pref_name);
        sharedPrefs = getSharedPreferences(sharedPrefName, MODE_PRIVATE);
        storageIdent = getResources().getString(R.string.shared_pref_key_storage);
        serviceStateIdent = getResources().getString(R.string.shared_pref_key_service_state);

        isInternal = sharedPrefs.getBoolean(storageIdent, true);
        isServiceEnabled = sharedPrefs.getBoolean(serviceStateIdent, true);
    }

    public void initRadioButtons() {

        if (isInternal) storageGroup.check(R.id.rb_internal_storage);
        else storageGroup.check(R.id.rb_external_storage);

        if (isServiceEnabled) serviceGroup.check(R.id.rb_service_enabled);
        else serviceGroup.check(R.id.rb_service_disabled);
    }

    @OnCheckedChanged({R.id.rb_internal_storage, R.id.rb_external_storage, R.id.rb_service_enabled, R.id.rb_service_disabled})
    public void onRadioButtonCheckChanged(CompoundButton button, boolean checked) {
        SharedPreferences.Editor editor = sharedPrefs.edit();
//        ExtendedBroadcastReceiver alarm;
        if (checked) {
            switch (button.getId()) {
                case R.id.rb_internal_storage:
                    editor.putBoolean(storageIdent, true);
                    break;
                case R.id.rb_external_storage:
                    editor.putBoolean(storageIdent, false);
                    break;
                case R.id.rb_service_enabled:
                    editor.putBoolean(serviceStateIdent, true);
//                     alarm = new ExtendedBroadcastReceiver(getApplicationContext(), 10);
                    break;
                case R.id.rb_service_disabled:
                    editor.putBoolean(serviceStateIdent, false);
//                    alarm = new ExtendedBroadcastReceiver(getApplicationContext(), 10);
//                    alarm.disableReceiver(getApplicationContext());
                    break;
            }
        }
        editor.commit();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
