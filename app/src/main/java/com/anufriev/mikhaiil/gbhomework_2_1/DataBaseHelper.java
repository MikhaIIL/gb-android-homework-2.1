package com.anufriev.mikhaiil.gbhomework_2_1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_CITY_NAME = "city_name";
    public static final String COLUMN_CITY_CODE = "city_code";
    public static final String COLUMN_ICON_ID = "weather_icon_id";
    public static final String COLUMN_DESCRIPTION = "weather_description";
    public static final String COLUMN_TEMP = "temperature";
    public static final String COLUMN_PRESS = "pressure";
    public static final String COLUMN_WIND_SPEED = "wind_speed";
    public static final String COLUMN_CALC_DATE = "calc_date";
    static final String WEATHER_INFO_TABLE = "weather_data";
    private static final String DATABASE_NAME = "weather_app_db.db";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + WEATHER_INFO_TABLE + " ( "
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_CITY_NAME + " VARCHAR(512) NOT NULL, "
                + COLUMN_CITY_CODE + " INTEGER NOT NULL, "
                + COLUMN_ICON_ID + " VARCHAR(256), "
                + COLUMN_DESCRIPTION + " VARCHAR(2000), "
                + COLUMN_TEMP + " DOUBLE, "
                + COLUMN_PRESS + " DOUBLE, "
                + COLUMN_WIND_SPEED + " DOUBLE, "
                + COLUMN_CALC_DATE + " DATETIME);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + WEATHER_INFO_TABLE);
        onCreate(sqLiteDatabase);
    }
}
